import {
  attr_config_nodeTypes,
  attr_domBindProps,
  attr_indexId,
  elem_args_keep_immutable,
} from "../const";
import { List as IList, Map as IMap } from "immutable";
import { createElement } from "react";

export function mkComponentOpts(optsImmutableMap) {
  return elem_args_keep_immutable.reduce(
    (jsOpts, attr) => {
      const found = optsImmutableMap.get(attr)
      if (found) {
        jsOpts[attr] = found
      }
      return jsOpts
    },
    optsImmutableMap.deleteAll(elem_args_keep_immutable).toJS()
  )
}

export function renderTree(config, tree) {
  const nodeTypes = config.get(attr_config_nodeTypes)
  if (!IList.isList(tree)) {
    console.warn("Tree: encountered bad node:", tree)
    return null
  }
  if (tree.size === 0) {
    throw new Error("Tree: Can't render: " + tree)
  }
  const nodeName = tree.get(0)
  const userOpts = tree.size > 1
    ? mkComponentOpts(tree.get(1))
    : {}
  const fullOpts = {
    // key should go first to allow override by the user
    key: userOpts[attr_domBindProps].get(attr_indexId),
    ...userOpts,
  }
  const Component = nodeTypes.get(nodeName)
  if (!Component) {
    console.warn("Tree: Unknown component:", nodeName)
    return null
  }
  if (tree.size > 2) {
    fullOpts.children = tree.slice(2).map(item => {
      return renderTree(config, item)
    })
  }
  return createElement(Component, fullOpts)
}
