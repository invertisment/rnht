import { createContext, useContext, useState, createElement, useEffect, useRef } from "react";
import { treeParent } from "./treeParent";
import { index, mkRandId } from '../dom/tree';
import { fromJS, List as IList, Map as IMap } from "immutable";
import { patch, replaceIds, runFnOnNodeOpts, select } from "../dom/swapOOB"

export const TreeContext = createContext(null)

export function doSwapOOB(config, mkId, origTreeAndIndex, oobForm) {
  const opts = oobForm.get(1)
  const tagSelector = opts.get("target")
  const swapIn = oobForm.get(2)
  const updated = patch(config, mkId, tagSelector, origTreeAndIndex, swapIn)
  return updated
}

export function doSwapById(config, mkId, origTreeAndIndex, swapForm) {
  const opts = swapForm.get(1)
  const targetIds = opts.get("targetIds")
  const swapIn = swapForm.get(2)
  return replaceIds(config, mkId, targetIds, origTreeAndIndex, swapIn)
}

export function deepMergeNodeOpts(origTreeAndIndex, form) {
  const opts = form.get(1)
  const target = opts.get("target")
  const newOpts = opts.get("value")
  return runFnOnNodeOpts(
    oldOpts => oldOpts.mergeDeep(newOpts),
    select(target, origTreeAndIndex),
    origTreeAndIndex,
  )
}

export const useTree = () => useContext(TreeContext)

export const WithTree = (props) => {
  const treeAndIndex_storage = useRef(
    index(
      props.config,
      mkRandId,
      IMap(),
      IList(),
      fromJS(props.initialRawTree)))
  const [ownTreeAndIndex, setCurrentTreeAndIndex] = useState(treeAndIndex_storage.current);
  useEffect(
    () => {
      const saveTreeFn = (swappedTreeAndIndex) => {
        // this useEffect callback is cached. But useRef's value is not.
        // Accessing useRef's value means that we
        // always take the newest item (because JS is single-threaded).
        treeAndIndex_storage.current = swappedTreeAndIndex
        setCurrentTreeAndIndex(swappedTreeAndIndex)
      }
      const cleanupSwapOobBatch = treeParent.addOnSwapOobBatchListener(oobForms => {
        saveTreeFn(
          oobForms.reduce(
            (treeAndIndex, form) => {
              return doSwapOOB(
                props.config,
                mkRandId,
                treeAndIndex,
                form)
            },
            treeAndIndex_storage.current,
          ))
      })
      const cleanupSwapByIdBatch = treeParent.addOnSwapByIdBatchListener(swapForms => {
        saveTreeFn(
          swapForms.reduce(
            (treeAndIndex, form) => {
              return doSwapById(
                props.config,
                mkRandId,
                treeAndIndex,
                form)
            },
            treeAndIndex_storage.current,
          ))
      })
      const cleanupDeepMergeOpts = treeParent.addOnNodeOptsDeepMergeBatchListener(deepMergeOptsLi => {
        saveTreeFn(
          deepMergeOptsLi.reduce(
            (treeAndIndex, optsForm) => deepMergeNodeOpts(treeAndIndex, optsForm),
            treeAndIndex_storage.current,
          ),
        )
      })
      return () => {
        cleanupSwapOobBatch()
        cleanupSwapByIdBatch()
        cleanupDeepMergeOpts()
      }
    },
    [])
  return createElement(
    TreeContext.Provider,
    {
      value: {
        treeAndIndex: ownTreeAndIndex,
        config: props.config,
      },
      children: props.children,
    })
}
