import {
  attr_domBindProps, attr_indexId,
  attr_name,
  attr_nodeName
} from "../const"

function hasInput(name, inputRef) {
  return name && inputRef
}

export class DOM_Boundary {

  optsHierarchy
  inputs = []

  constructor(optsHierarchy) {
    this.optsHierarchy = optsHierarchy
  }

  getParentOpts() {
    return this.optsHierarchy[this.optsHierarchy.length - 1]
  }

  ////////// input and other registration //////////

  regSelfAsChild(props, inputFieldRef, validateAndShowInputValueError) {
    const name = props[attr_name]
    const opts = props[attr_domBindProps]
    if (hasInput(name, inputFieldRef)) {
      this.inputs.push({
        name: name,
        ref: inputFieldRef,
        validateField: validateAndShowInputValueError,
      })
    }
    return () => {
      if (hasInput(name, inputFieldRef)) {
        this.inputs = this.inputs.filter(({ ref }) => ref !== inputFieldRef)
      }
    }
  }

  gatherInputValues() {
    return this.inputs.reduce(
      (out, { name, ref }) => {
        out[name] = ref.current
        return out
      },
      {})
  }

  validateAndShowInputValueErrors() {
    const allInputs = this.gatherInputValues()
    const allErrors = this.inputs.reduce(
      (out, { validateField }) => {
        if (!validateField) {
          return out
        }
        return out.concat(validateField(allInputs))
      },
      [])
    return allErrors.length === 0
  }

  ////////// derivation //////////

  mkChildBoundary(parentOpts) {
    if (!parentOpts) {
      throw new Error("ID must be provided for each child boundary")
    }
    return new DOM_Boundary(
      [...this.optsHierarchy, parentOpts],
    )
  }
}
