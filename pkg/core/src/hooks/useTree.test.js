import { index } from "../dom/tree"
import { List as IList, Map as IMap, fromJS } from "immutable"
import { mkSequentialIndexFn } from "../dom/tree.test";
import { deepMergeNodeOpts, doSwapById, doSwapOOB } from "./useTree"

const indexedTree = index(
  IMap({ "my": "config" }),
  mkSequentialIndexFn(1000),
  IMap(),
  IList(),
  fromJS(
    ["view", { __tags: ["a"] },
      ["view", { __tags: ["b"] },
        ["text", { __tags: ["c"], "children": ["hello"] }],
      ]]))

const treePatch = fromJS(
  ["text", { "children": ["it works"] }]);

describe('swap oob form options test', () => {
  it('walk and replace maps with "opts" + set "path"', () => {
    const treeResult = doSwapOOB(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(9999),
      indexedTree,
      fromJS(["something", { "target": "b" }, treePatch]))
    expect(treeResult[0].toJS()).toEqual(
      ["view",
        {
          "__domBindProps": {
            "config": { "my": "config" },
            "indexId": "1002",
            "nodeName": "view",
            "path": []
          },
          "__tags": ["a"]
        },
        ["text",
          {
            "__domBindProps": {
              "config": { "my": "config" },
              "indexId": "9999",
              "nodeName": "text",
              "path": [2]
            },
            "children": ["it works"]
          }]]
    );
  });
});


describe('doSwapById form options test', () => {
  it('target and replace maps with "it works"', () => {
    const treeResult = doSwapById(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(9999),
      indexedTree,
      fromJS(["something", { "targetIds": ["1001"] }, treePatch]))
    expect(treeResult[0].toJS()).toEqual(
      ["view",
        {
          "__domBindProps": {
            "config": { "my": "config" },
            "indexId": "1002",
            "nodeName": "view",
            "path": []
          },
          "__tags": ["a"]
        },
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "config": {
              "my": "config",
            },
            "indexId": "9999",
            "nodeName": "text",
            "path": [2,],
          },
        }]],
    );
  });
});

describe('deepMergeNodeOpts form options test', () => {
  it('should find and merge opts', () => {
    const treeResult = deepMergeNodeOpts(
      indexedTree,
      fromJS(
        ["something",
          {
            "target": "b",
            value: {
              style: { hi: 123 },
              __tags: ["my-new-tag"],
            }
          },
        ]))
    expect(treeResult[0].toJS()).toEqual(
      ["view",
        {
          "__domBindProps": {
            "config": { "my": "config" },
            "indexId": "1002",
            "nodeName": "view",
            "path": []
          },
          "__tags": ["a"]
        },
        ["view",
          {
            "__domBindProps": {
              "config": { "my": "config" },
              "indexId": "1001",
              "nodeName": "view",
              "path": [2]
            },
            "__tags": ["b", "my-new-tag"],
            "style": { hi: 123 }
          },
          ["text",
            {
              "__domBindProps": {
                "config": { "my": "config" },
                "indexId": "1000",
                "nodeName": "text",
                "path": [2, 2]
              },
              "__tags": ["c"],
              "children": ["hello"]
            }]]]
    );
  });
});


