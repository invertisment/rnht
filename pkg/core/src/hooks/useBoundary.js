import { createContext, useContext } from "react";
import { DOM_Boundary } from "./boundary";
import { createElement } from "react";

export function deriveCtx(ctx, childProps) {
  return ctx.mkChildBoundary(childProps)
}

export const BoundaryContext = createContext(new DOM_Boundary([{}]))

export const useBoundaryContext = () => useContext(BoundaryContext)

export const WrapChildren = (props) => {
  return createElement(BoundaryContext.Provider, {
    value: props.ctx,
    children: props.children
  })
}
