import { mkActionsFromProps } from "../interaction/actions";
import {
  attr_config,
  attr_config_transformComponentOptsFn,
} from "../const";

function identity(item) { return item }

export function mkOpts(parentCtx, props) {
  const transformComponentOptsFn = props.__domBindProps.getIn(
    [attr_config, attr_config_transformComponentOptsFn],
    identity
  )
  return transformComponentOptsFn({
    ...props,
    ...mkActionsFromProps(parentCtx, props)
  })
}
