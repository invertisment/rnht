import { noLog } from "../testutil";
import { DOM_Boundary } from "./boundary";
import { Map as IMap } from "immutable";

describe('DOM_Boundary', () => {
  it('constructor should construct the obj', () => {
    var eventMap = { myEvent: () => { } }
    var out = new DOM_Boundary(["hello", "there"], eventMap);
    expect(out.optsHierarchy).toEqual(["hello", "there"]);
  });
  it('getParentOpts should return last item in the parent data list', () => {
    var eventMap = { myEvent: () => { } }
    var out = new DOM_Boundary(["hello", "there"], eventMap);
    expect(out.getParentOpts()).toEqual("there");
  });
  describe('mkChildBoundary', () => {
    it('mkChildBoundary should keep the event map', () => {
      const eventMap = { myEvent: () => { } }
      const parent = new DOM_Boundary(["parent"], eventMap);
      const childBoundary = parent.mkChildBoundary("child")
      expect(childBoundary.optsHierarchy).toEqual(["parent", "child"]);
    });
    it('should throw when no child id is provided', () => {
      const eventMap = { myEvent: () => { } }
      const parent = new DOM_Boundary(["parent"], eventMap);
      try {
        parent.mkChildBoundary()
        fail("expected to throw")
      } catch (_ignored) { }
    });
  });

  describe('regSelfAsChild', () => {
    it('register and unregister [one param]', () => {
      noLog(() => {
        const eventMap = { myEvent: () => { } }
        const parent = new DOM_Boundary(["parent"], eventMap);
        const cleanupFn = parent.regSelfAsChild({
          __domBindProps: IMap({
            indexId: "123",
            nodeName: "view",
          })
        })
        cleanupFn()
      })
    });
    it('register and unregister [with input field]', () => {
      noLog(() => {
        const eventMap = { myEvent: () => { } }
        const parent = new DOM_Boundary(["parent"], eventMap);
        const unmountFn = parent.regSelfAsChild(
          {
            __name: "my-input",
            __domBindProps: IMap({
              indexId: "123",
              nodeName: "view",
            })
          },
          { current: "1337.0" })
        expect(parent.gatherInputValues()).toEqual({
          "my-input": "1337.0"
        });
        unmountFn()
        expect(parent.gatherInputValues()).toEqual({});
      });
    });
    it('register two inputs', () => {
      noLog(() => {
        const eventMap = { myEvent: () => { } }
        const parent = new DOM_Boundary(["parent"], eventMap);
        const unmountFn1 = parent.regSelfAsChild(
          {
            __name: "my-input",
            __domBindProps: IMap({
              indexId: "123",
              nodeName: "view",
            })
          },
          { current: "1337.0" })
        const unmountFn2 = parent.regSelfAsChild(
          {
            __name: "my-input-2",
            __domBindProps: IMap({
              indexId: "124",
              nodeName: "view",
            })
          },
          { current: 12431 })
        expect(parent.gatherInputValues()).toEqual({
          "my-input": "1337.0",
          "my-input-2": 12431
        });
        unmountFn1()
        expect(parent.gatherInputValues()).toEqual({
          "my-input-2": 12431
        });
        unmountFn2()
        expect(parent.gatherInputValues()).toEqual({});
      });
    });

    describe('register and validate an input', () => {
      it('no validation fn', () => {
        noLog(() => {
          const eventMap = { myEvent: () => { } }
          const parent = new DOM_Boundary(["parent"], eventMap);
          parent.regSelfAsChild(
            {
              __domBindProps: IMap({
                indexId: "123",
                nodeName: "view",
                __name: "my-input",
              })
            },
            { current: "1337.0" },
          )
          expect(parent.validateAndShowInputValueErrors()).toEqual(true);
        });
      });
      it('invalid single input [invalid]', () => {
        noLog(() => {
          const eventMap = { myEvent: () => { } }
          const parent = new DOM_Boundary(["parent"], eventMap);
          parent.regSelfAsChild(
            {
              __name: "my-input",
              __domBindProps: IMap({
                indexId: "123",
                nodeName: "view",
              })
            },
            { current: "1337.0" },
            () => false)
          expect(parent.validateAndShowInputValueErrors()).toEqual(false);
        });
      });

      it('should pass all values as a map', () => {
        noLog(() => {
          const output = []
          const eventMap = { myEvent: () => { } }
          const parent = new DOM_Boundary(["parent"], eventMap);
          parent.regSelfAsChild(
            {
              __name: "my-input",
              __rules: [["my-rule"]],
              __domBindProps: IMap({
                indexId: "123",
                nodeName: "view",
              })
            },
            { current: "1337.0" },
            (values) => {
              output.push(values)
              return false
            })
          parent.validateAndShowInputValueErrors()
          expect(output).toEqual([{ "my-input": "1337.0" }]);
        });
      });
    });
  });
});
