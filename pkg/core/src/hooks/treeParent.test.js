import { noErrorLog } from "../testutil";
import { _TreeParent } from "./treeParent";
import { fromJS } from "immutable";

describe('TreeParent class', () => {
  it('add a listener and trigger it', () => {
    const log = []
    const instance = new _TreeParent()
    instance.addOnSwapOobBatchListener(a => {
      log.push(a)
    })
    instance.swapOobBatch("hello world")
    instance.swapOobBatch("hi")
    expect(log).toEqual(["hello world", "hi"]);
  });
  it('cleanup a listener and trigger it', () => {
    const log = []
    const instance = new _TreeParent()
    const cleanupFn1 = instance.addOnSwapOobBatchListener(a => {
      log.push(a + " 1")
    })
    instance.addOnSwapOobBatchListener(a => {
      log.push(a + " 2")
    })
    instance.swapOobBatch("hello")
    cleanupFn1()
    instance.swapOobBatch("hi")
    expect(log).toEqual(["hello 1", "hello 2", "hi 2"]);
  });
  it("thrown exception shouldn't stop listeners", () => {
    const log = []
    const instance = new _TreeParent()
    instance.addOnSwapOobBatchListener(a => {
      log.push(a + " 1")
      throw new Error("hi")
    })
    instance.addOnSwapOobBatchListener(a => {
      log.push(a + " 2")
    })
    noErrorLog(() => {
      instance.swapOobBatch("hello")
      instance.swapOobBatch("hi")
    })
    expect(log).toEqual(["hello 1", "hello 2", "hi 1", "hi 2"]);
  });
  it('oob without batch should execute batch with one item', () => {
    const log = []
    const instance = new _TreeParent()
    const cleanupFn1 = instance.addOnSwapOobBatchListener(a => {
      log.push(a + " 1")
    })
    instance.addOnSwapOobBatchListener(a => {
      log.push(a + " 2")
    })
    instance.swapOob("hello")
    cleanupFn1()
    instance.swapOob("hi")
    expect(log).toEqual([
      "List [ \"hello\" ] 1",
      "List [ \"hello\" ] 2",
      "List [ \"hi\" ] 2"]);
  });
});

describe('swap by id', () => {
  it('swapById should notify swap listeners', () => {
    const log = []
    const instance = new _TreeParent()
    const cleanupFn1 = instance.addOnSwapByIdBatchListener(a => {
      log.push(a + " 1")
    })
    instance.addOnSwapByIdBatchListener(a => {
      log.push(a + " 2")
    })
    instance.swapByInternalIdBatch("hello")
    cleanupFn1()
    instance.swapByInternalId("hi")
    expect(log).toEqual([
      "hello 1",
      "hello 2",
      "List [ \"hi\" ] 2"]);
  });
});

describe('merge option', () => {
  it('mergeNodeOption should notify listeners', () => {
    const log = []
    const instance = new _TreeParent()
    const cleanupFn1 = instance.addOnNodeOptsDeepMergeBatchListener(
      optsLi => { log.push(optsLi) }
    )
    instance.addOnNodeOptsDeepMergeBatchListener(
      optsLi => { log.push(optsLi) }
    )
    instance.mergeNodeOptions({ style: { hello: 123 } })
    cleanupFn1()
    instance.mergeNodeOptions({ style: { hello: 124 } })
    expect(log).toEqual([
      fromJS([{
        style: { hello: 123 },
      }]),
      fromJS([{
        style: { hello: 123 },
      }]),
      fromJS([{
        style: { hello: 124 },
      }])
    ]);
  });
});
