import { fromJS } from "immutable"

export class _TreeParent {
  swap_oob_batch_listeners = []
  swap_by_id_listeners = []
  opts_deepmerge_listeners = []

  _invokeListeners(listeners, form_mut) {
    const form = fromJS(form_mut)
    listeners.forEach(l => {
      try {
        l(form)
      } catch (e) {
        console.error(e)
      }
    })
  }

  addOnSwapOobBatchListener(l) {
    this.swap_oob_batch_listeners.push(l)
    return () => {
      this.swap_oob_batch_listeners = this.swap_oob_batch_listeners.filter(
        found => found !== l
      )
    }
  }
  swapOob(form) {
    this.swapOobBatch([form])
  }
  swapOobBatch(form_mut) {
    this._invokeListeners(this.swap_oob_batch_listeners, form_mut)
  }

  addOnSwapByIdBatchListener(l) {
    this.swap_by_id_listeners.push(l)
    return () => {
      this.swap_by_id_listeners = this.swap_by_id_listeners.filter(
        found => found !== l
      )
    }
  }
  swapByInternalId(form) {
    this.swapByInternalIdBatch([form])
  }
  swapByInternalIdBatch(form_mut) {
    this._invokeListeners(this.swap_by_id_listeners, form_mut)
  }

  addOnNodeOptsDeepMergeBatchListener(l) {
    this.opts_deepmerge_listeners.push(l)
    return () => {
      this.opts_deepmerge_listeners = this.opts_deepmerge_listeners.filter(
        found => found !== l
      )
    }
  }
  mergeNodeOptions(optsOverrideCmd) {
    this.mergeNodeOptionsBatch([optsOverrideCmd])
  }
  mergeNodeOptionsBatch(optsOverrideCmds) {
    this._invokeListeners(this.opts_deepmerge_listeners, optsOverrideCmds)
  }
}

export const treeParent = new _TreeParent()
