
export async function noLog(f) {
  const oldLog = console.log
  console.log = jest.fn()
  try {
    await f()
  } finally {
    console.log = oldLog
  }
}

export async function noErrorLog(f) {
  const oldLog = console.error
  console.error = jest.fn()
  try {
    await f()
  } finally {
    console.error = oldLog
  }
}
