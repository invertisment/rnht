import { postwalk, postwalkPath } from "./walk";
import { fromJS, List as IList, Map as IMap } from "immutable";

const tree = fromJS(
  ["view", { __tags: ["b"], style: { backgroundColor: "green", padding: 5, display: "flex" } },
    ["button", { "title": "hello there" }],
    ["textinput", { style: { backgroundColor: "white", display: "none" } }],
    ["text", { "children": ["hello there"] }]]);

describe('postwalk', () => {
  it('walk and replace maps with "opts"', () => {
    const treeResult = postwalk(
      form => {
        if (IMap.isMap(form)) {
          return "opts"
        }
        return form
      },
      tree)
    expect(treeResult.toJS()).toEqual(
      ["view", "opts",
        ["button", "opts"],
        ["textinput", "opts"],
        ["text", "opts"]]);
  });

  it('should allow reduce ops [count leaf nodes]', () => {
    const treeResult = postwalk(
      form => {
        if (IList.isList(form)) {
          return form.reduce((out, item) => out + item)
        }
        return 1
      },
      tree)
    expect(treeResult).toEqual(8);
  });


  it('walk should only walk elements once', () => {
    const order = []
    postwalk(
      (form) => {
        if (IList.isList(form)) {
          order.push(form.get(0))
        }
        return form
      },
      tree)
    expect(order).toEqual(["button", "textinput", "text", "view"]);
  });
});

describe('postwalkPath', () => {
  it('walk and replace maps with "opts"', () => {
    const treeResult = postwalkPath(
      (form, path) => {
        if (IList.isList(form)) {
          return form
        }
        return path.toJS()
      },
      IList(),
      tree)
    expect(treeResult).toEqual(
      IList([[0], [1],
      IList([[2, 0], [2, 1]]),
      IList([[3, 0], [3, 1]]),
      IList([[4, 0], [4, 1]]),
      ]));
  });

  it('should allow to start at arbitrary path', () => {
    const treeResult = postwalkPath(
      (form, path) => {
        if (IList.isList(form)) {
          return form
        }
        return path.toJS()
      },
      IList([9, 9, 1]),
      tree)
    expect(treeResult).toEqual(
      IList([[9, 9, 1, 0], [9, 9, 1, 1],
      IList([[9, 9, 1, 2, 0], [9, 9, 1, 2, 1]]),
      IList([[9, 9, 1, 3, 0], [9, 9, 1, 3, 1]]),
      IList([[9, 9, 1, 4, 0], [9, 9, 1, 4, 1]]),
      ]));
  });
});
