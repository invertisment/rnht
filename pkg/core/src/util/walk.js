import { List as IList } from "immutable";

export function postwalkPath(
  fn,
  path,
  form,
) {
  return fn(
    IList.isList(form)
      ? form.map((formItem, idx) => postwalkPath(
        fn,
        path.push(idx),
        formItem))
      : form,
    path)
}

export function postwalk(
  fn,
  form
) {
  return fn(
    IList.isList(form)
      ? form.map(formItem => postwalk(fn, formItem))
      : form
  )
}

export function prewalkVisit(
  fn,
  form
) {
  fn(form)
  if (IList.isList(form)) {
    form.forEach(formItem => prewalkVisit(fn, formItem))
  }
}
