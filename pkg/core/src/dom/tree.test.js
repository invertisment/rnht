import { fromJS, List as IList, Map as IMap, Set as ISet } from "immutable";
import { findChildIDs, index, updateOpts } from "./tree";
import { attr_config } from "../const";

const smallTree = fromJS(
  ["view", { style: { backgroundColor: "green", padding: 5, display: "flex" } },
    ["button", { "title": "hello there" }],
  ]);

describe('updateOpts', () => {
  it('should add id nodes to the tree', () => {
    const newTree = updateOpts(
      IMap({ "my config prop": "my fns" }),
      smallTree,
      "my-id",
      IList([1, 2, 3, 56])
    )
    expect(newTree.toJS()).toEqual(
      ["view",
        {
          __domBindProps: {
            "indexId": "my-id",
            "nodeName": "view",
            "path": [1, 2, 3, 56],
            "config": { "my config prop": "my fns" },
          },
          "style": {
            "backgroundColor": "green",
            "display": "flex",
            "padding": 5
          }
        },
        ["button",
          { "title": "hello there" }]]);
  });
});

export function mkSequentialIndexFn(initialValue) {
  var i = initialValue
  return () => {
    return (i++).toString()
  }
}

export function mkRepeatingNumberValuesFn(
  initialValue,
  repetitions
) {
  var i = initialValue
  var repeats = 0
  return () => {
    if (repeats < repetitions) {
      repeats += 1
      return (i).toString()
    }
    repeats = 0
    return (i++).toString()
  }
}

describe('index', () => {
  it('should construct index of each node', () => {
    const [updatedTree, domIndex] = index(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(0),
      IMap(),
      IList(),
      smallTree,
    )
    expect(domIndex.keySeq().toSet()).toEqual(
      ISet([
        "0",
        "1",
      ])
    );
    expect(domIndex.valueSeq().toSet()).toEqual(
      ISet([
        IList([]),
        IList([2]),
      ])
    );
    expect(updatedTree.toJS()).toEqual(
      ["view",
        {
          "style": {
            "backgroundColor": "green",
            "display": "flex",
            "padding": 5
          },
          __domBindProps: {
            "nodeName": "view",
            "indexId": "1",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["button",
          {
            "title": "hello there",
            __domBindProps: {
              "nodeName": "button",
              "indexId": "0",
              "path": [2],
              "config": { "my": "config" },
            },
          }]]);
  });

  it("should retry picking node's index if it clashes", () => {
    const [updatedTree, domIndex] = index(
      IMap({ "my": "config" }),
      mkRepeatingNumberValuesFn(25, 5),
      IMap(),
      IList(),
      smallTree,
    )
    expect(domIndex.keySeq().toSet()).toEqual(
      ISet([
        "25",
        "26",
      ])
    );
    expect(domIndex.valueSeq().toSet()).toEqual(
      ISet([
        IList([]),
        IList([2]),
      ])
    );
    expect(updatedTree.toJS()).toEqual(
      ["view",
        {
          "style": {
            "backgroundColor": "green",
            "display": "flex",
            "padding": 5
          },
          __domBindProps: {
            "nodeName": "view",
            "indexId": "26",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["button",
          {
            "title": "hello there",
            __domBindProps: {
              "nodeName": "button",
              "indexId": "25",
              "path": [2],
              "config": { "my": "config" },
            },
          }]]);
  });

  it("should check ID existence in additional taken keys map", () => {
    const [_updatedTree, domIndex] = index(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(25),
      IMap({
        "25": "taken",
        "26": "taken",
        "27": "taken",
      }),
      IList(),
      smallTree
    )
    expect(domIndex.keySeq().toSet()).toEqual(
      ISet([
        "28",
        "29",
      ])
    );
  });
});


describe('findChildIDs', () => {
  it('should return a list of IDs', () => {
    const indexedTree = index(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(5),
      IMap(),
      IList(),
      smallTree,
    )[0]
    const ids = findChildIDs(indexedTree)
    expect(ids.sort()).toEqual(
      ["5", "6"]
    );
  });
});
