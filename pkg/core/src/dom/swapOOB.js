import { prewalkVisit } from "../util/walk";
import { findChildIDs, index } from "./tree";
import { List as IList, Map as IMap } from 'immutable';
import { path_domID, attr_tags } from "../const";

export function select(
  destinationTag,
  tree,
) {
  const idResult = []
  prewalkVisit(
    form => {
      if (!IList.isList(form)) {
        return
      }
      const opts = form.get(1)
      if (!IMap.isMap(opts)) {
        return
      }
      const tags = opts.get(attr_tags);
      if (!tags || !tags.find(t => t === destinationTag)) {
        return
      }
      const domBindId = opts.getIn(path_domID)
      idResult.push(domBindId)
      return
    },
    tree[0])
  return IList(idResult)
}

export function getNodeId(nodeOpts) {
  return nodeOpts.getIn(path_domID)
}

export function hasExpectedId(opts, expectedId) {
  return getNodeId(opts) === expectedId
}

export function updateSpecificId(
  targetId,
  tree,
  updateFn,
) {
  var usedIndexNodes = tree[1]
  const path = usedIndexNodes.get(targetId)
  try {
    const newDOMTree = tree[0].updateIn(
      path,
      (form) => {
        const formOpts = form.get(1)
        if (!hasExpectedId(formOpts, targetId)) {
          return form
        }
        const [subtree, updatedUsedIndexNodes] = updateFn(
          form,
          usedIndexNodes,
          path,
        )
        usedIndexNodes = updatedUsedIndexNodes
        return subtree
      })
    return [newDOMTree, usedIndexNodes]
  } catch (e) {
    return tree
  }
}

export function updateSpecificIds(
  targetIds,
  tree,
  updateFn,
) {
  return targetIds.reduce(
    (outTree, targetId) => {
      return updateSpecificId(targetId, outTree, updateFn)
    },
    tree
  )
}

export function replaceIds(
  config,
  mkId,
  targetIds,
  tree,
  swapIn
) {
  return updateSpecificIds(
    targetIds,
    tree,
    (form, usedIndexNodes, path) => {
      const [subtree, subIndex] = index(
        config,
        mkId,
        usedIndexNodes,
        path,
        swapIn,
      )
      return [
        subtree,
        usedIndexNodes
          .removeAll(findChildIDs(form))
          .merge(subIndex),
      ]
    },
  )
}

export function patch(
  config,
  mkId,
  destinationTag,
  tree,
  swapIn,
) {
  return replaceIds(config, mkId, select(destinationTag, tree), tree, swapIn)
}

export function runFnOnNodeOpts(
  f,
  targetIds,
  tree,
) {
  return updateSpecificIds(
    targetIds,
    tree,
    (form, usedIndexNodes, _path) => {
      return [
        form.update(1, f),
        usedIndexNodes,
      ]
    },
  )
}
