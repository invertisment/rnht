import {
  attr_domBindProps, path_domID, attr_nodeName,
  attr_indexId, attr_path, attr_config
} from "../const";
import { postwalk, postwalkPath } from "../util/walk";
import { List as IList, Map as IMap } from "immutable"

export function mkRandId() {
  return Math.random().toString().substring(2)
}

export function updateOpts(config, tree, indexId, path) {
  return tree.updateIn(
    [1, attr_domBindProps],
    _currentOpts => IMap([
      [attr_nodeName, tree.get(0)],
      [attr_indexId, indexId],
      [attr_path, path],
      [attr_config, config],
    ])
  )
}

function pickUnusedIdx(
  alreadyTaken,
  obj,
  mkId
) {
  var indexId = mkId()
  while (obj[indexId] || alreadyTaken.get(indexId)) {
    indexId = mkId()
  }
  return indexId
}

export function index(
  config,
  mkId,
  alreadyUsedIdx,
  treeAtPath,
  tree
) {
  const mut_index = {}
  const updatedTree = postwalkPath(
    (form, path) => {
      if (IList.isList(form)) {
        const nodeId = pickUnusedIdx(alreadyUsedIdx, mut_index, mkId);
        mut_index[nodeId] = path;
        return updateOpts(config, form, nodeId, path)
      }
      return form
    },
    treeAtPath,
    tree)
  return [updatedTree, IMap(mut_index)]
}

export function findChildIDs(tree) {
  return postwalk(
    form => {
      if (!IList.isList(form)) {
        return form
      }
      const opts = form.get(1)
      if (!IMap.isMap(opts)) {
        return form
      }
      const id = opts.getIn(path_domID)
      return form.slice(2).map(findChildIDs).reduce((out, item) => {
        return item.concat(out)
      }, [id])
    },
    tree
  )
}
