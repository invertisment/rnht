import { fromJS, List as IList, Map as IMap } from "immutable";
import { patch, replaceIds, select, runFnOnNodeOpts } from "./swapOOB";
import { index } from "./tree";
import { mkSequentialIndexFn } from "./tree.test";

const indexedTree = index(
  IMap({ "my": "config" }),
  mkSequentialIndexFn(1000),
  IMap(),
  IList(),
  fromJS(
    ["view", { __tags: ["a"] },
      ["view", { __tags: ["b"] },
        ["text", { __tags: ["c"], "children": ["hello"] }],
      ]]))

const indexedTreeTwoTargets = index(
  IMap({ "my": "config" }),
  mkSequentialIndexFn(1000),
  IMap(),
  IList(),
  fromJS(
    ["view", { __tags: ["a"] },
      ["view", { __tags: ["b"] },],
      ["view", { __tags: ["b"] },]
    ]))

const treePatch = fromJS(
  ["text", { "children": ["it works"] }]);

describe('original tree', () => {
  it('original tree', () => {
    expect(indexedTree[0].toJS()).toEqual(
      ["view",
        {
          __tags: ["a"],
          "__domBindProps": {
            "indexId": "1002",
            "nodeName": "view",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["view", {
          __tags: ["b"],
          "__domBindProps": {
            "indexId": "1001",
            "nodeName": "view",
            "path": [2],
            "config": { "my": "config" },
          }
        },
          ["text", {
            __tags: ["c"], "children": ["hello"],
            "__domBindProps": {
              "indexId": "1000",
              "nodeName": "text",
              "path": [2, 2],
              "config": { "my": "config" },
            }
          }]]]);
  });
  it('original index', () => {
    expect(indexedTree[1].toJS()).toEqual(
      {
        "1000": [2, 2,],
        "1001": [2,],
        "1002": [],
      });
  });
});

describe('patch', () => {
  it('walk and replace maps with "opts" + set "path"', () => {
    const treeResult = patch(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(9999),
      "b",
      indexedTree,
      treePatch)
    expect(treeResult[0].toJS()).toEqual(
      ["view",
        {
          __tags: ["a"],
          "__domBindProps": {
            "indexId": "1002",
            "nodeName": "view",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "indexId": "9999",
            "nodeName": "text",
            "path": [2],
            "config": { "my": "config" },
          }
        }]]);
  });
  it('walk and replace maps with "opts" + set "path"', () => {
    const result = patch(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(9999),
      "b",
      indexedTree,
      treePatch)
    expect(result[1].toJS()).toEqual(
      {
        "9999": [2,],
        "1002": [],
      });
  });
  it('should not reuse IDs', () => {
    const result = patch(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(1002),
      "b",
      indexedTree,
      treePatch)
    expect(result[0].toJS()).toEqual(
      ["view",
        {
          __tags: ["a"],
          "__domBindProps": {
            "indexId": "1002",
            "nodeName": "view",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "indexId": "1003",
            "nodeName": "text",
            "path": [2],
            "config": { "my": "config" },
          }
        }]]);
    expect(result[1].toJS()).toEqual(
      {
        "1003": [2,],
        "1002": [],
      });
  });

  it('should swap two items in one step', () => {
    const treeResult = patch(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(9999),
      "b",
      indexedTreeTwoTargets,
      treePatch)
    expect(treeResult[0].toJS()).toEqual(
      ["view",
        {
          __tags: ["a"],
          "__domBindProps": {
            "indexId": "1002",
            "nodeName": "view",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "indexId": "9999",
            "nodeName": "text",
            "path": [2],
            "config": { "my": "config" },
          }
        }],
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "indexId": "10000",
            "nodeName": "text",
            "path": [3],
            "config": { "my": "config" },
          }
        }]
      ]);
  });
  it('walk and replace maps with "opts" + set "path"', () => {
    const result = patch(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(9999),
      "b",
      indexedTreeTwoTargets,
      treePatch)
    expect(result[1].toJS()).toEqual(
      {
        "10000": [3,],
        "9999": [2,],
        "1002": [],
      });
  });
});

describe('select', () => {
  it('walk and replace maps with "opts" + set "path"', () => {
    const ids = select("b", indexedTree)
    expect(ids.toJS()).toEqual(["1001"]);
  });
});

describe('replaceIds', () => {
  it('walk and replace maps with "opts" + set "path"', () => {
    const result = replaceIds(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(1002),
      IList(["1001"]),
      indexedTree,
      treePatch,
    )
    expect(result[0].toJS()).toEqual(
      ["view",
        {
          __tags: ["a"],
          "__domBindProps": {
            "indexId": "1002",
            "nodeName": "view",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "indexId": "1003",
            "nodeName": "text",
            "path": [2],
            "config": { "my": "config" },
          }
        }]]);
  });

  it('should maintain the ID index', () => {
    const result = replaceIds(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(1002),
      IList(["1001"]),
      indexedTree,
      treePatch,
    )
    expect(result[1].toJS()).toEqual(
      {
        "1003": [2,],
        "1002": [],
      });
  });

  it('check that ID actually matches before the swap', () => {
    const corruptedIndex = IMap({
      "1001": IList([])
    })
    const result = replaceIds(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(1002),
      IList(["1001"]),
      [indexedTree[0], corruptedIndex],
      treePatch,
    )
    expect(result).toEqual([indexedTree[0], corruptedIndex])
  });

  it('should swap two items in one step', () => {
    const treeResult = replaceIds(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(5555),
      IList(["1000", "1001"]),
      indexedTreeTwoTargets,
      treePatch)
    expect(treeResult[0].toJS()).toEqual(
      ["view",
        {
          __tags: ["a"],
          "__domBindProps": {
            "indexId": "1002",
            "nodeName": "view",
            "path": [],
            "config": { "my": "config" },
          },
        },
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "indexId": "5555",
            "nodeName": "text",
            "path": [2],
            "config": { "my": "config" },
          }
        }],
        ["text", {
          "children": ["it works"],
          "__domBindProps": {
            "indexId": "5556",
            "nodeName": "text",
            "path": [3],
            "config": { "my": "config" },
          }
        }]
      ]);
  });

  it('should ignore non-found IDs', () => {
    const result = replaceIds(
      IMap({ "my": "config" }),
      mkSequentialIndexFn(1111),
      IList(["123414"]),
      indexedTree,
      treePatch,
    )
    expect(result[1].toJS()).toEqual(
      {
        "1000": [2, 2,],
        "1001": [2,],
        "1002": [],
      });
  });

  describe('merge opts attribute', () => {
    it("should accept map and create when it doesn't exist", () => {
      const result = runFnOnNodeOpts(
        (currentValue) => currentValue.update(
          "style",
          (styleOpts) => {
            if (!styleOpts) {
              return fromJS({
                value1: 123,
                value2: 124,
              })
            }
            return styleOpts.merge({
              value1: 123,
              value2: 124,
            })
          }),
        ["1000"],
        indexedTree,
      )
      expect(result[0].toJS()).toEqual(
        ["view", {
          "__domBindProps": {
            "config": { "my": "config" },
            "indexId": "1002",
            "nodeName": "view",
            "path": []
          },
          "__tags": ["a"]
        },
          ["view",
            {
              "__domBindProps": {
                "config": { "my": "config" },
                "indexId": "1001",
                "nodeName": "view",
                "path": [2]
              },
              "__tags": ["b"]
            },
            ["text",
              {
                "style": {
                  "value1": 123,
                  "value2": 124,
                },
                "__domBindProps": {
                  "config": { "my": "config" },
                  "indexId": "1000",
                  "nodeName": "text",
                  "path": [2, 2]
                },
                "__tags": ["c"],
                "children": ["hello"]
              }]]]);
    });
    it("should accept map and create when it doesn't exist", () => {
      const updateFn = (styleOpts) => {
        if (!styleOpts) {
          return fromJS({
            value1: 123,
            value2: 124,
          })
        }
        return styleOpts.merge({
          value2: 125,
          value3: 126,
        })
      }
      const result1 = runFnOnNodeOpts(
        (currentValue) => currentValue.update("style", updateFn),
        ["1000"],
        indexedTree,
      )
      const result2 = runFnOnNodeOpts(
        (currentValue) => currentValue.update("style", updateFn),
        ["1000"],
        result1,
      )
      expect(result2[0].toJS()[2][2]).toEqual(
        ["text",
          {
            "style": {
              value1: 123,
              value2: 125,
              value3: 126,
            },
            "__domBindProps": {
              "config": { "my": "config" },
              "indexId": "1000",
              "nodeName": "text",
              "path": [2, 2]
            },
            "__tags": ["c"],
            "children": ["hello"]
          }]);
    });
  });
});
