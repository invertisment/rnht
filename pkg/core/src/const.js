// element attrs
export const attr_tags = "__tags"
export const attr_actions = "__actions"
export const attr_domBindProps = "__domBindProps"
export const attr_name = "__name"
export const attr_validate = "__validate"
export const attr_onSubmit = "__onSubmit"

// attrs inside __domBindProps
export const attr_indexId = "indexId"
export const attr_nodeName = "nodeName"
export const attr_path = "path"
export const attr_config = "config"
export const path_domID = [attr_domBindProps, attr_indexId]

// config attrs
export const attr_config_nodeTypes = "nodeTypes"
export const attr_config_interpreterFns = "interpreterFns"
export const attr_config_validators = "validators"
export const attr_config_transformComponentOptsFn = "transformComponentOptsFn"

export const err_debugTag = "[RNHT] "

export const elem_args_keep_immutable = [
  attr_tags,
  attr_actions,
  attr_domBindProps,
  attr_name,
  attr_validate,
  attr_onSubmit,
]
