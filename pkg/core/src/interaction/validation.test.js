import { noErrorLog } from "../testutil";
import { validate } from "./validation";
import { fromJS } from "immutable";

const sampleConfig = fromJS({
  validators: {
    alwaysFalse: () => ["always fail"],
    alwaysTrue: () => [],
    alwaysThrow: () => { throw new Error("something really bad") },
    eqField: (form, value, allValues) => {
      return value === allValues[form.get(1)]
        ? []
        : ["fields must match"]
    },
  }
})

describe('validate', () => {
  it('should return "ok" on empty', () => {
    const out = validate(sampleConfig, [], null, null)
    expect(out).toEqual([]);
  });
  it('should ignore unknown validator', () => {
    const out = validate(
      sampleConfig,
      fromJS([
        ["unknown"],
      ]),
      null,
      null)
    expect(out).toEqual([]);
  });
  it('should ignore undefined validator', () => {
    const out = validate(
      sampleConfig,
      undefined,
      null,
      null)
    expect(out).toEqual([]);
  });
  it('should validate according to config', () => {
    const out = validate(
      sampleConfig,
      fromJS([
        ["alwaysFalse"],
        ["alwaysTrue"],
      ]),
      null,
      null)
    expect(out).toEqual(["always fail"]);
  });
  it('should return empty errors on error', () => {
    noErrorLog(() => {
      const out = validate(
        sampleConfig,
        fromJS([
          ["alwaysTrue"],
          ["alwaysThrow"],
        ]),
        null,
        null)
      expect(out).toEqual([]);
    })
  });
  it('should equal field', () => {
    noErrorLog(() => {
      const out = validate(
        sampleConfig,
        fromJS([
          ["eqField", "other-field"],
        ]),
        12345,
        { "other-field": 12345 })
      expect(out).toEqual([]);
    })
  });
  it('should equal field [not equal]', () => {
    noErrorLog(() => {
      const out = validate(
        sampleConfig,
        fromJS([
          ["eqField", "other-field"],
        ]),
        10000,
        { "other-field": 12345 })
      expect(out).toEqual(["fields must match"]);
    })
  });
  it('should not crash on non-immutable rule as input', () => {
    const out = validate(
      sampleConfig,
      [
        ["alwaysFalse"],
        ["alwaysTrue"],
      ],
      null,
      null)
    expect(out).toEqual(["always fail"]);
  });
});
