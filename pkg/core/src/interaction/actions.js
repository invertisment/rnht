import { Map as IMap, List as IList, fromJS } from "immutable";
import {
  attr_actions,
  attr_config,
  attr_config_interpreterFns,
  attr_domBindProps,
  attr_indexId,
  err_debugTag,
} from "../const"

export function evaluate(fnScope, context, nodeId, form, evalFn_CtxNodeFormEv) {
  if (!IList.isList(form)) {
    return form
  }
  const title = form.get(0)
  const actionFn = fnScope.get(title)
  if (!actionFn) {
    console.error(err_debugTag + "Unknown action '" + title + "'")
    return
  }
  return actionFn(context, nodeId, form, evalFn_CtxNodeFormEv)
}

export function mkActions(fnScope, context, nodeId, elemActionMap) {
  if (!elemActionMap) {
    return {}
  }
  const evalFn_CtxNodeFormEv = (context, nodeId, form, evalFn_CtxNodeFormEv) => {
    return evaluate(fnScope, context, nodeId, form, evalFn_CtxNodeFormEv)
  }
  return elemActionMap.entrySeq().reduce(
    (out, [k, v]) => {
      out[k] = async () => await evalFn_CtxNodeFormEv(context, nodeId, v, evalFn_CtxNodeFormEv)
      return out
    },
    {})
}

export function mkActionsFromProps(context, compProps) {
  return mkActions(
    compProps[attr_domBindProps]
      .getIn([attr_config, attr_config_interpreterFns]),
    context,
    compProps[attr_domBindProps].get(attr_indexId),
    compProps[attr_actions],
  )
}
