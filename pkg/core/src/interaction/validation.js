import { fromJS } from "immutable"
import { attr_config_validators } from "../const"

export function validate(config, validationTuples, value, allValues) {
  const validators = config.get(attr_config_validators)
  if (!validationTuples) {
    return []
  }
  return fromJS(validationTuples).reduce(
    (li, tuple) => {
      const name = tuple.get(0)
      const validatorFn = validators.get(name)
      if (!validatorFn) {
        return li
      }
      try {
        return li.concat(validatorFn(tuple, value, allValues))
      } catch (e) {
        console.error(e)
        return li
      }
    },
    [])
}
