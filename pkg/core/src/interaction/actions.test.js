import { noErrorLog } from "../testutil";
import { mkActions, mkActionsFromProps } from "./actions";
import { Map as IMap, List as IList, fromJS } from "immutable";

function mkFnScope() {
  const callbackResults = []
  return [
    IMap({
      testFn: () => { callbackResults.push("testFn") },
      testFn2: () => { callbackResults.push("testFn2") },
    }),
    callbackResults
  ]
}

const sampleBoundary = {}

describe('mkActions', () => {
  it('should create action attributes', () => {
    const actions = mkActions(
      mkFnScope()[0],
      sampleBoundary,
      "1337",
      IMap({
        myAction: IList(["testFn"]),
        mySecondAction: IList(["hi"])
      }))
    expect(Object.keys(actions).sort())
      .toEqual(["myAction", "mySecondAction"]);
  });
  it('should call the underlying callback fn', async () => {
    const [actionConfig, result] = mkFnScope()
    const actions = mkActions(
      actionConfig,
      sampleBoundary,
      "1337",
      IMap({ myAction: IList(["testFn"]) }))
    await actions.myAction()
    expect(result).toEqual(["testFn"]);
  });
  it('should distinguish between two actions', async () => {
    const [actionConfig, result] = mkFnScope()
    const actions = mkActions(
      actionConfig,
      sampleBoundary,
      "1337",
      IMap({
        myAction: IList(["testFn"]),
        myActionTwo: IList(["testFn2"]),
      }))
    await actions.myAction()
    await actions.myAction()
    await actions.myActionTwo()
    expect(result).toEqual(["testFn", "testFn", "testFn2"]);
  });
  it('should not crash on no action fn', async () => {
    await noErrorLog(async () => {
      const input = ["testFn"]
      console.error = jest.fn()
      const actions = mkActions(
        IMap(),
        sampleBoundary,
        "1337",
        IMap({ myAction: IList(input) }))
      await actions.myAction()
      expect(console.error).toHaveBeenCalledWith(
        "[RNHT] Unknown action 'testFn'"
      )
    })
  });
  it('should return when input is not a call', async () => {
    await noErrorLog(async () => {
      const actions = mkActions(
        IMap(),
        sampleBoundary,
        "1337",
        IMap({ myAction: "My input" }))
      const output = await actions.myAction()
      expect(output).toEqual("My input")
    })
  });
});

describe('mkActionsFromProps', () => {
  it('should create action attributes', () => {
    const actions = mkActionsFromProps(
      sampleBoundary,
      {
        __domBindProps: fromJS({
          config: { interpreterFns: mkFnScope()[0] },
          indexId: "1337",
        }),
        __actions: fromJS({
          myAction: ["testFn"],
          mySecondAction: ["hi"]
        })
      })
    expect(Object.keys(actions).sort())
      .toEqual(["myAction", "mySecondAction"]);
  });
});
