import DOM_Button from './src/components/DOM_Button';
import DOM_View from './src/components/DOM_View';
import DOM_Text from './src/components/DOM_Text';
import DOM_TextInput from './src/components/DOM_TextInput';
import { DOM_Tree, Config, DOM_TreeRaw, AdditionalCompProps } from './src/components/types';
import { fromJS, List as IList, Map as IMap } from "immutable";
import RNHT from './src/components/RNHT';
import { treeParent } from "rntree_core/src/hooks/treeParent";
import { DOM_Boundary } from "rntree_core/src/hooks/boundary";
import { Button, ScrollView, View, Text } from 'react-native';
import DOM_Form from './src/components/DOM_Form';
import {
  attr_indexId,
  attr_onSubmit,
} from "rntree_core/src/const";
import { WrapGrid, transformComponentOptsForGrid } from './hooks/useGrid';

async function doExpr(b: DOM_Boundary, nodeId: string, forms: DOM_Tree[], evaluate: any) {
  var currentPromise = Promise.resolve(undefined)
  forms.forEach(innerForm => {
    currentPromise = currentPromise.then(() => {
      return evaluate(b, nodeId, innerForm, evaluate)
    })
  })
  return await currentPromise
}

const config: Config = fromJS({
  nodeTypes: {
    button: DOM_Button,
    view: DOM_View,
    text: DOM_Text,
    textinput: DOM_TextInput,
    form: DOM_Form,
  },
  // These functions are called "functions" but they're special.
  // They are macro-time functions which retrieve arguments in an "unevaluated"
  // form.
  // Normal functions in "normal programming languages" retrieve arguments as
  // already evaluated and you can't implement your own "if" statements.
  //
  // Exposing macro functions from the interpreter allows to implement any type
  // of control flow structure such as "if" or "for".
  // I discourage implementing them because you use this framework to get
  // away from JavaScript -- but I won't stop you.
  //
  // The programming in these macro forms is not ergonomic but this library
  // doesn't expect you to have a lot of these rather than combine them so this
  // is why it's exposed this way.
  //
  // If one wishes to have "normal function-like behavior" then one may
  // implement a helper builder function that will just evaluate each arg
  // (just declare it somewhere in your utils and call it).
  interpreterFns: {
    // `arguments` variable is not present in arrow functions
    "print-args": function() { console.info("[ACTION] print-args", arguments) },
    "print": async (b: DOM_Boundary, nodeId: string, form: DOM_Tree, evaluate: any) => {
      const evalResult = await evaluate(b, nodeId, form.get(1), evaluate)
      console.log(evalResult)
      return evalResult
    },
    "swap-oob": (_b: DOM_Boundary, _nodeId: string, form: DOM_Tree, _e: any) => {
      treeParent.swapOob(form)
    },
    "swap": async (b: DOM_Boundary, internalNodeId: string, form: DOM_Tree, evaluate: any) => {
      treeParent.swapByInternalId(
        ["swap",
          { "targetIds": [internalNodeId] },
          await evaluate(b, internalNodeId, form.get(2), evaluate)])
    },
    // Actions and tree notation is tree-like lists.
    // This means that the system can't distinguish when to evaluate it as a list
    // and when to swap it as a node.
    // If a list has to be returned as a parameter but is already evaluated
    // then this function helps to keep it in the "macro time" instead of
    // evaluating the list as an action.
    "as-literal": (_b: DOM_Boundary, _nodeId: string, form: DOM_Tree, _evaluate: any) => {
      return form.get(1)
    },
    "identity": (_b: DOM_Boundary, _nodeId: string, form: DOM_Tree, _e: any) => {
      return form
    },
    "form-validate": (b: DOM_Boundary, _nodeId: string, _form: DOM_Tree, _e: any) => {
      return b.validateAndShowInputValueErrors()
    },
    "when": async (b: DOM_Boundary, nodeId: string, form: DOM_Tree, evaluate: any) => {
      if (await evaluate(b, nodeId, form.get(1), evaluate)) {
        return await doExpr(b, nodeId, (form as any).skip(2), evaluate)
      }
    },
    "form-gather": (b: DOM_Boundary, _nodeId: string, _form: DOM_Tree, _e: any) => {
      return b.gatherInputValues()
    },
    "merge-style": (_b: DOM_Boundary, _nodeId: string, form: DOM_Tree, _e: any) => {
      const opts = form.get(1) as IMap<string, any>
      const target = opts.get("target")
      const value = opts.get("value")
      return treeParent.mergeNodeOptions(["", { target: target, value: { style: value } }])
    },
    "form-submit": async (b: DOM_Boundary, _nodeId: string, _form: DOM_Tree, evaluate: any) => {
      const componentOpts: AdditionalCompProps = b.getParentOpts()
      const onSubmitForm = componentOpts[attr_onSubmit]
      const formNodeId = componentOpts.__domBindProps.get(attr_indexId)
      if (onSubmitForm) {
        return await evaluate(b, formNodeId, onSubmitForm, evaluate)
      }
    },
    "POST": async (b: DOM_Boundary, nodeId: string, form: DOM_Tree, evaluate: any) => {
      const url = form.get(1)
      const body = await evaluate(b, nodeId, form.get(2), evaluate)
      console.log("POST", url, body, form.get(2))
      return new Promise((resolve, _reject) => {
        return setTimeout(() =>
          resolve(["text", { children: ["Form was posted. Form's whole container was swapped with this text."] }]),
          2000)
      })
    }
  },
  validators: {
    eqField: (form: IList<any>, value: any, allValues: object) => {
      return (
        value === allValues[form.get(1)]
          ? []
          : [form.get(2)]
      )
    },
    nomatch: (form: IList<any>, value: any, _allValues: object) => {
      return (
        new RegExp(form.get(1)).test(value)
          ? []
          : [form.get(2)]
      )
    },
  },
  transformComponentOptsFn: (opts: any) => {
    return transformComponentOptsForGrid(opts)
  }
}) as any

function block(...body: DOM_TreeRaw[]): DOM_TreeRaw {
  return ["view",
    {
      style: [{
        backgroundColor: "antiquewhite",
        borderStyle: "solid",
        borderWidth: 1,
        margin: 5,
        padding: 10,
        display: "flex"
      }]
    },
  ].concat(body as any) as DOM_TreeRaw
}

const inputTree2: DOM_TreeRaw =
  ["view", {
    style: {
      backgroundColor: "green",
      padding: 5,
      display: "flex",
      borderStyle: "dashed",
      borderWidth: 1,
      borderColor: "white",
    }
  },
    ["button", {
      __tags: ["b"],
      "title": "hello there",
      __actions: {
        onPress: ["print-args", "two"],
      }
    }],
    ["textinput", { __name: "added-input", }],
    ["text", { "children": ["hello there"] }]]


const inputVisible: DOM_TreeRaw =
  ["textinput", { __name: "input2", __tags: ["b"], style: { backgroundColor: "antiquewhite", display: "flex" } }];

const inputHidden: DOM_TreeRaw =
  ["textinput", { __name: "input3", __tags: ["b"], style: { backgroundColor: "white", display: "none" } }];

const inputGone: DOM_TreeRaw =
  ["view", { __tags: ["b"], style: { backgroundColor: "white", display: "none" } }];

const treeSwapOOB: DOM_TreeRaw = block(
  ["view", { __tags: ["a"] },
    ["text", { children: "Swap the red container with a green one." }],
    ["button", {
      "title": "Swap OOB!",
      __actions: {
        onPress: ["swap-oob", { "target": "b" }, inputTree2],
      }
    },],
    //["button", {
    //  "title": "Input: Replace with visible",
    //  __actions: {
    //    onPress: ["swap-oob", { "target": "b" }, inputVisible],
    //  }
    //},],
    //["button", {
    //  "title": "Input: Replace with hidden",
    //  __actions: {
    //    onPress: ["swap-oob", { "target": "b" }, inputHidden],
    //  }
    //},],
    //["button", {
    //  "title": "Remove input",
    //  __actions: {
    //    onPress: ["swap-oob", { "target": "b" }, inputGone],
    //  }
    //},],
    ["view", { __tags: ["b"], style: { backgroundColor: "orangered", padding: 5, display: "flex" } },
      ["button", {
        "title": "hello",
        __actions: {
          onPress: ["print-args", "one"],
        }
      }],
      ["text", { "children": ["hello"] }],
    ]]);

const treeForm: DOM_TreeRaw = block(
  ["form",
    {
      __onSubmit:
        ["when", ["form-validate"],
          ["merge-style",
            { target: "my-form-loading-overlay", value: { display: "flex" } }],
          ["swap", {}, ["POST", "https://example.com", ["form-gather"]]],
          ["merge-style",
            { target: "my-form-loading-overlay", value: { display: "none" } }],
        ],
      style: {},
    },
    ["text", { "children": ["Form is validated client-side before sending it to POST function (which you could implement yourself and actually send data)"] }],
    ["textinput", {
      __name: "input4",
      "style": { "backgroundColor": "lightgreen", margin: 5, gap: 5 },
      __rules: [
        ["nomatch", "^.{5,}$", "Length must be at least 5 symbols"],
        ["eqField", "input5", "Must be equal to the bottom lightgreen input"]
      ],
    }],
    ["textinput", {
      __name: "input5",
      defaultValue: "aaaaa",
      "style": { "backgroundColor": "lightgreen", margin: 5, gap: 5 },
      __rules: [["eqField", "input4", "Must be equal to another lightgreen input"]],
    }],
    ["textinput", {
      __name: "input6",
      inputMode: "numeric",
      "style": { "backgroundColor": "greenyellow", margin: 5, gap: 5 },
      __rules: [
        ["nomatch", "^\\d+$", "Must be a number"],
      ],
    }],
    ["button", {
      "title": "Validate form and print to console",
      __actions: {
        onPress: ["when", ["form-validate"], ["print", ["form-gather"]]],
      }
    }],
    ["button", {
      "title": "Validate form and submit (waits 2 seconds)",
      __actions: {
        onPress: ["form-submit"],
      }
    }],
    ["view", { __tags: ["b"], }],
    ["view",
      {
        __tags: ["my-form-loading-overlay"],
        style: {
          display: "none",
          position: "absolute",
          height: "100%",
          width: "100%",
          backgroundColor: "#fffc",
          justifyContent: "center",
          alignItems: "center"
        }
      },
      ["text", { "children": ["Loading"], }]
    ],
  ])

const treeResponsive: DOM_TreeRaw = block(
  ["text", { "children": ["Grid is implemented entirely in example application and not in core."] }],
  ["text", { "children": ["Change the window width to see it in action."] }],
  ["view",
    { style: { backgroundColor: "antiquewhite" }, },
    ["text", { "children": ["at least 2xs"], __grid: { "2xs": { backgroundColor: "chartreuse" } } }],
    ["text", { "children": ["at least xs"], __grid: { "xs": { backgroundColor: "chartreuse" } } }],
    ["text", { "children": ["at least sm"], __grid: { "sm": { backgroundColor: "chartreuse" } } }],
    ["text", { "children": ["at least md"], __grid: { "md": { backgroundColor: "chartreuse" } } }],
    ["text", { "children": ["at least lg"], __grid: { "lg": { backgroundColor: "chartreuse" } } }],
    ["text", { "children": ["at least xl"], __grid: { "xl": { backgroundColor: "chartreuse" } } }],
    ["text", { "children": ["at least 2xl"], __grid: { "2xl": { backgroundColor: "chartreuse" } } }],
  ],
)

const treeSwapSelf: DOM_TreeRaw = block(
  ["text", { "children": ["swap the button that is pressed!"] }],
  ["view", { style: { backgroundColor: "lightcyan", padding: 5 } },
    ["text", { "children": ["before"] }],
    ["button", {
      "title": "swap me out!",
      __actions: {
        onPress: ["swap", {}, ["as-literal", inputTree2]],
      }
    }],
    ["text", { "children": ["after"] }]])

const treeChangeColor: DOM_TreeRaw = block(
  ["text", { "children": ["merge-style deep-merges into the style property of the target node."] }],
  ["view",
    {
      __tags: ["color-tag"],
      style: { backgroundColor: "white", padding: 20, gap: 10 }
    },
    ["button", {
      "title": "Paint Cyan",
      __actions: {
        onPress: ["merge-style", {
          target: "color-tag",
          value: { backgroundColor: "cyan" }
        },],
      }
    }],
    ["button", {
      "title": "Paint Chartreuse",
      __actions: {
        onPress: ["merge-style", {
          target: "color-tag",
          value: { backgroundColor: "chartreuse" }
        },],
      }
    }]],
)

export default function App() {
  //console.log("tree", treeAndIndex[0].toJS())
  //console.log("index", treeAndIndex[1].toJS())
  return <WrapGrid>
    <ScrollView style={{ width: "100%", height: "auto" }}>
      <View style={{
        backgroundColor: "antiquewhite",
        borderStyle: "solid",
        borderWidth: 1,
        margin: 5,
        padding: 10,
        display: "flex",
        gap: 5,
      }} >
        <Text>Externally activated controls (not from inside of the tree buttons)</Text>
        <Button title='Swap oob from outside' onPress={() => {
          treeParent.swapOob(["swap-oob", { "target": "b" }, inputTree2])
        }} />
        <Button title='Swap oob x2 (two updates in one batch)' onPress={() => {
          treeParent.swapOobBatch([
            ["swap-oob", { "target": "b" }, inputTree2],
            ["swap-oob", { "target": "b" }, inputTree2],
          ])
        }} />
      </View>
      <RNHT config={config} rawTree={treeSwapOOB} />
      <RNHT config={config} rawTree={treeSwapSelf} />
      <RNHT config={config} rawTree={treeForm} />
      <RNHT config={config} rawTree={treeResponsive} />
      <RNHT config={config} rawTree={treeChangeColor} />
    </ScrollView>
  </WrapGrid>
}
