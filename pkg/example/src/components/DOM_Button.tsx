import { Button } from "react-native";
import { AdditionalCompProps } from "./types";
import { useBoundaryContext } from "rntree_core/src/hooks/useBoundary";
import { mkOpts } from "rntree_core/src/hooks/opts";
import { useEffect } from "react";

export default function(props: AdditionalCompProps & any) {
  const parentCtx = useBoundaryContext()

  const opts = mkOpts(parentCtx, props)

  useEffect(() => {
    return parentCtx.regSelfAsChild(props)
  }, [props.__domBindProps.indexId])
  return <Button {...opts} />
}
