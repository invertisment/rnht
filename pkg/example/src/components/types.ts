import { List as IList, Map as IMap } from "immutable"
import { DOM_Boundary } from "rntree_core/src/hooks/boundary";

export type DOM_TreeRaw =
  [string, object]
  |
  [string, object, ...DOM_TreeRaw[]];

// type:
// [title, map]
// [title, map, recursive node...]
export type DOM_Tree =
  IList<string | IMap<string, any>>
  |
  IList<string | IMap<string, any> | DOM_Tree>;

export type DOM_TreeIndex = IMap<string, IList<number>>

export type DOM_TreeAndIndex = [DOM_Tree, DOM_TreeIndex]

export type NodeTypes = IMap<string, MyComponent>
export type InterpereterFns = IMap<string, () => any>
export type Config = IMap<string, any>

export type AdditionalCompProps = {
  __domBindProps: { // IMap
    nodeName: string,
    indexId: string,
    path: IList<number>,
    config: Config,
  },
  __rules?: IList<IList<string>>,
  __name?: string,
}

export type MyComponent = (
  | React.ComponentClass<AdditionalCompProps, unknown>
  | React.FunctionComponent<AdditionalCompProps>
);

export interface DOM_Ctx {
  domBoundary: DOM_Boundary,
}

export type WithTreeProps = {
  initialTree: DOM_Tree,
  config: Config,
  children: any,
}
