import { Config, DOM_TreeRaw } from './types';
import { renderTree } from "rntree_core/src/render/render";
import { WithTree, useTree } from 'rntree_core/src/hooks/useTree';

function RenderTree() {
  const { treeAndIndex, config } = useTree()
  return renderTree(config, treeAndIndex[0])
}

export default function(props: {
  config: Config,
  rawTree: DOM_TreeRaw,
}) {
  return <WithTree
    initialRawTree={props.rawTree}
    config={props.config} >
    <RenderTree />
  </WithTree>
}
