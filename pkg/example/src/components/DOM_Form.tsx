import { View, ViewProps } from "react-native";
import { WrapChildren, useBoundaryContext, deriveCtx } from "rntree_core/src/hooks/useBoundary";
import { AdditionalCompProps } from "./types";
import { useEffect, useMemo } from "react";
import { mkOpts } from "rntree_core/src/hooks/opts";

export default function(props: AdditionalCompProps & ViewProps) {
  const parentCtx = useBoundaryContext()
  const childCtx = useMemo(
    () => deriveCtx(parentCtx, props),
    [props.__domBindProps.indexId, parentCtx])
  useEffect(() => {
    return parentCtx.regSelfAsChild(props)
  }, [props.__domBindProps.indexId])

  const opts = mkOpts(parentCtx, props)

  return <View {...opts} >
    <WrapChildren ctx={childCtx}>
      {props.children}
    </WrapChildren>
  </View>
}
