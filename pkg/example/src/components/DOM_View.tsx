import { View, ViewProps } from "react-native";
import { useBoundaryContext } from "rntree_core/src/hooks/useBoundary";
import { AdditionalCompProps } from "./types";
import { useEffect } from "react";
import { mkOpts } from "rntree_core/src/hooks/opts";

export default function(props: AdditionalCompProps & ViewProps) {
  const parentCtx = useBoundaryContext()
  useEffect(() => {
    return parentCtx.regSelfAsChild(props)
  }, [props.__domBindProps.indexId])

  const opts = mkOpts(parentCtx, props)

  return <View {...opts} >
    {props.children}
  </View>
}
