import { Text, TextProps } from "react-native";
import { WrapChildren, useBoundaryContext, deriveCtx } from "rntree_core/src/hooks/useBoundary";
import { AdditionalCompProps } from "./types";
import { useEffect, useMemo } from "react";
import { mkOpts } from "rntree_core/src/hooks/opts";

export default function(props: AdditionalCompProps & TextProps) {
  const parentCtx = useBoundaryContext()
  useEffect(() => {
    return parentCtx.regSelfAsChild(props)
  }, [props.__domBindProps.indexId])

  const opts = mkOpts(parentCtx, props)

  return <Text {...opts} >
    {props.children}
  </Text>
}
