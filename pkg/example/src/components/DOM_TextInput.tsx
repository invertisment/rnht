import { TextInput, TextInputProps, View, Text } from "react-native";
import { useBoundaryContext } from "rntree_core/src/hooks/useBoundary";
import { AdditionalCompProps } from "./types";
import { useEffect, useRef, useState } from "react";
import { validate } from "rntree_core/src/interaction/validation";
import { mkOpts } from "rntree_core/src/hooks/opts";

export default function(props: AdditionalCompProps & TextInputProps) {

  const value = useRef(props.defaultValue || "")
  const [errorTexts, setErrorTexts] = useState(props.__errors || [])

  const parentCtx = useBoundaryContext()
  useEffect(() => {
    return parentCtx.regSelfAsChild(props, value, (allValues: object) => {
      const newErrors = validate(
        props.__domBindProps.get("config"),
        props.__rules,
        value.current,
        allValues,
      )
      setErrorTexts(newErrors)
      return newErrors
    })
  }, [props.__domBindProps.indexId])

  const opts = mkOpts(parentCtx, props)

  return <View style={opts.style}>
    <TextInput {...opts} onChangeText={text => {
      setErrorTexts([])
      value.current = text
    }} />
    {errorTexts.length !== 0 && errorTexts.map(t => <Text key={t}>{t}</Text>)}
  </View>
}
