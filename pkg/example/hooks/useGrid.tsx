import { useMemo } from 'react';
import { useWindowDimensions } from 'react-native';
import { createContext, useContext } from "react";

// This is the feature in the example application itself.
const __myGridAttr = "__grid"

const windowWidths = {
  "2xs": 384,
  "xs": 512,
  // Threshold source: https://tailwindcss.com/docs/responsive-design
  "sm": 640,
  "md": 768,
  "lg": 1024,
  "xl": 1280,
  "2xl": 1536,
}
const responsiveThresholds = ["2xs", "xs", "sm", "md", "lg", "xl", "2xl"]

function calcAtWidth(currentWidth: number) {
  return Object.entries(windowWidths).reduce(
    (grid, [key, atWidth]) => {
      grid[key] = currentWidth >= atWidth
      return grid
    },
    {},
  )
}

export const GridContext = createContext(null)

export const useGrid = () => useContext(GridContext)

export const WrapGrid = (props: any) => {
  const dimensions = useWindowDimensions()
  const value = useMemo(() => {
    return calcAtWidth(dimensions.width)
  }, [JSON.stringify(calcAtWidth(dimensions.width))])
  return <GridContext.Provider
    value={value}
    children={props.children} />
}

export function transformComponentOptsForGrid(opts: any) {
  const grid = useGrid()
  const gridStyles = opts[__myGridAttr] || {}
  opts.style = responsiveThresholds.reduce(
    (finalStyleList, threshold) => {
      if (!grid[threshold]) {
        return finalStyleList
      }
      const foundGridStyle = gridStyles[threshold]
      if (!gridStyles[threshold]) {
        return finalStyleList
      }
      finalStyleList.push(foundGridStyle)
      return finalStyleList
    },
    [opts.style])
  return opts
}
