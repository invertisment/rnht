
## React Native Hyper Text

Expo example run instructions:

```
cd pkg/example
yarn
yarn start
```

## Goals
- Data structure as input; no imperative code
- data structure is HTML-like AST and not necessarily HTML
- Maintainable DOM from outside -- Have something similar to `hx-swap-oob`
- Select and replace nodes by Class
- Node's context instead of hard-coded form implementation (state retrieve via getter functions)
- Single hook for easy custom element in-tree registration
- UI is responsive in the client side (styles)
- Render from outside (no inside fetch)
- Allow custom user elements
- Web compatibility (via RN web)

## Non-goals
- Render whole app in one data structure
- Any relation to client side routing except via user's component's own implementation
- Coupling to a Routing lib
- Link handling (user has to define how they want it to work and where to store its state)
- Type safety outside the main hook
- ~~Handle out of bounds tree swap of a part of a screen which has unmounted views~~

## Avoid
- TypeScript doesn't import with modules and fails on browser. Workaround -- use JS.
- TSX doesn't work with modules. Workaround -- create elements by hand.
- Becoming a "non-boring" "everything but a kitchen-sink" library
- Assume a stupid user that needs "protection"

## Scrapped ideas
- Global event system. HTMX has an event system that allows to hook into its lifecycles and interact with request flow (e.g. `htmx:abort`). This doesn't seem to be as powerful as allowing the user to bind the `onPress` functions themselves and add their own function primitives. In addition, the current implementation of layout tree is available all the time (no waiting) whereas the rendered React components are only fully constructed when everything finishes its rendering (`useEffect` binds to form a "DOM" tree). This means that any kind of event system would need to account for unmounted components and partial state whereas running a transaction on a tree will re-render the UI tree just fine. The [settling](https://htmx.org/docs/#request-operations) of HTMX has multiple stages (one of them involves a timeout), and it seems to be complexity that could be avoided. This doesn't mean that there will be any preventive measures against event systems -- it's just that the core doesn't need to ship with it.
- Use HTML.
It's easy to use "already established" but we already have established the use of JSON and the use of the data structures in the programming languages of your choice.
People don't code in HTML, but instead they think in data structures and this means that HTML is a worse medium of transfer than simply thinking about transferring a data structure.
On top of that it's also easy to compose data structures to build something out of them.
And it doesn't mean to use JSON as you could use MsgPack as this library doesn't force a transfer technology.

# Consistency model
Layout updates are [Strictly-Serializable](https://jepsen.io/consistency/models/strict-serializable).
Tree's transactions are short-lived and don't block.
They mostly replace/modify the data in the tree.

Forms and their value gathering are eventually consistent as "the stateful DOM-like tree" is formed only at React's hook binding time.
The binding happens fast, so it's negligible as the user won't be able to type in millisecond time.

Nodes are indexed by a random ID for faster lookup.
This ID is mostly invisible (except for "swap" operation of "self" element).
This ID must be populated from the tree structure by your backend when [using lists](https://react.dev/learn/rendering-lists#keeping-list-items-in-order-with-key).

[Persistent data structures](https://www.npmjs.com/package/immutable) are used to allow updating the tree without modifying the currently rendered version.

## Other
### Videos
[NPM worksapces](https://youtube.com/watch?v=4CpR-_Nyd00)
